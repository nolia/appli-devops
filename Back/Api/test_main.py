from Api.routes import app

def test_route_index():
  response = app.test_client().get('/')
  assert response.status_code == 200

def test_poulet():
  response = app.test_client().get('/poulet')
  assert response.status_code == 200

# def test_map():
#   response = app.test_client().get('/map')
#   assert response.status_code == 200

# def test_map_trajet():
#   response = app.test_client().get('/map/trajet')
#   assert response.status_code == 200

# def test_route_1():
#   response = app.test_client().get('/api/v1/etablissement')
#   assert response.status_code == 500

def test_route_css_1():
  response = app.test_client().get('/CSS/style.css')
  assert response.status_code == 200

def test_route_css_2():
  response = app.test_client().get('/CSS/all.min.css')
  assert response.status_code == 200

def test_route_css_3():
  response = app.test_client().get('/CSS/bootstrap.min.css')
  assert response.status_code == 200

def test_route_js_1():
  response = app.test_client().get('/JS/main.js')
  assert response.status_code == 200

def test_route_js_2():
  response = app.test_client().get('/JS/all.min.js')
  assert response.status_code == 200

def test_route_js_3():
  response = app.test_client().get('/JS/bootstrap.bundle.min.js')
  assert response.status_code == 200

def test_route_js_4():
  response = app.test_client().get('/JS/jquery.min.js')
  assert response.status_code == 200
