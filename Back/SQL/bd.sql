-- mysql -u root -proot bd
-- select * from etablissement;

CREATE DATABASE  IF NOT EXISTS `bd`;

USE `bd`;

DROP TABLE IF EXISTS `etablissement`;

CREATE TABLE `etablissement` (
  `ID_ETABLISSEMENT` int NOT NULL AUTO_INCREMENT,
  `NOM_ETABLISSEMENT` varchar(60) NOT NULL,
  `TYPE_ETABLISSEMENT` varchar(60) NOT NULL,
  `MAIL` varchar(60) NOT NULL,
  `TEL` varchar(60) DEFAULT NULL,
  `ADRESSE` varchar(60) DEFAULT NULL,
  `GEOLAT` decimal(8,6) DEFAULT NULL,
  `GEOLNG` decimal(8,6) DEFAULT NULL,
  `HORAIRE` varchar(60) DEFAULT NULL,
  `DESCRIPTION` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ID_ETABLISSEMENT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO etablissement(NOM_ETABLISSEMENT,TYPE_ETABLISSEMENT,MAIL,TEL,ADRESSE,GEOLAT,GEOLNG,HORAIRE,DESCRIPTION)
VALUES ("ugc", "divertissement", "cine@gmail.com", "0146372824", "40 Rue de B�thune 59800 Lille", "50.634060","3.063452",'11:00',"Un cin�ma"),
("kinepolis", "divertissement", "cine@gmail.com", "0320170400", "1 Rue Chateau Isenghien 59160 Lille", "50.651482","2.981057",'12:30',"Cin�ma"),
("grand palais", "culture", "musee@gmail.com", "0144131717", "3 Av du G�n�ral Eisenhower 75008 Paris", "48.867268","2.311889",'08:40',"Mus�e"),
("louvre", "culture", "musee@gmail.com", "0140205050", "Rue de Rivoli 75001 Paris","48.860783","2.341316", '09:00',"Le mus�e du Louvre");
