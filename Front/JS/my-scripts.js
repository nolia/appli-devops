/*!
* Start Bootstrap - Simple Sidebar v6.0.3 (https://startbootstrap.com/template/simple-sidebar)
* Copyright 2013-2021 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-simple-sidebar/blob/master/LICENSE)
*/
//
// Scripts
//

window.addEventListener('DOMContentLoaded', event => {

    // Toggle the side navigation
    const sidebarToggle = document.body.querySelector('#sidebarToggle');
    if (sidebarToggle) {
        // Uncomment Below to persist sidebar toggle between refreshes
        // if (localStorage.getItem('sb|sidebar-toggle') === 'true') {
        //     document.body.classList.toggle('sb-sidenav-toggled');
        // }
        sidebarToggle.addEventListener('click', event => {
            event.preventDefault();
            document.body.classList.toggle('sb-sidenav-toggled');
            localStorage.setItem('sb|sidebar-toggle', document.body.classList.contains('sb-sidenav-toggled'));
        });
    }

});



// -------------------------
//
// Accueil
//
// -------------------------


function accueil(){
  location.reload()
}


// -------------------------
//
// Fonctions de la carte
//
// -------------------------

function mapTrajet() {
  $(".accueil").hide();
  $(".ajout-form").hide();
  $(".etablissement-content").hide();
  $(".carte").show();
}

function calculTrajet() {
  type = $("#type_etablissement").val()
  let request = $.ajax({
    type: "GET",
    url: `http://localhost/api/v1/map/trajet?type=${type}`,
    dataType: "html",
    beforeSend: function () {},
  });

  request.done(function (response) {
    showOnlyContent(response);
  });

  request.fail(function (request) {
    casErreur(request);
  });

  request.always(function () {});
}


// ------------------------------------
//
// Fonctions pour les établissements
//
// ------------------------------------


// Fonction qui retourne tous les établissements de notre base de données
function etablissement() {
  let request = $.ajax({
    type: "GET",
    url: "http://localhost/api/v1/etablissement",
    dataType: "json",
    beforeSend: function () {},
  });

  request.done(function (response) {
    $(".accueil").hide();
    $(".ajout-form").hide()
    // Entête du tableau affichant la liste des établissements
    let html = `
                <div class="p-4">
                  <h3 class="p-4 text-center">Liste des établissements</h3>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                            <th scope="col">Nom</th>
                            <th scope="col">Type</th>
                            <th scope="col">Mail</th>
                            <th scope="col">Téléphone</th>
                            <th scope="col">Adresse</th>
                            <th scope="col">Description</th>
                            <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                          `;
    // On récupère les informations de la base de données et on les affiche
    response.reverse().map((etablissement) => {
      html += `<tr>
                <th scope="row">${etablissement.nom}</th>
                <td>${etablissement.type}</td>
                <td>${etablissement.mail}</td>
                <td>${etablissement.tel}</td>
                <td>${etablissement.adresse}</td>
                <td>${etablissement.description}</td>
                <td>
                  <button type="button" class="btn btn-primary px-4" onclick="afficheEtablissement(${etablissement.id})">  Voir  </button>
                  <button type="button" class="btn btn-info px-3" onclick="modifEtablissement(${etablissement.id})">  Modifier  </button>
                  <button type="button" class="btn btn-danger" onclick="suppEtablissement(${etablissement.id})">Supprimer</button>
                </td>
                </tr>
                <tr>
                  <td style="display: none;" colspan="7" class="affichageProfil numProfil${etablissement.id}"></td>
              </tr>
              `;
    });
    // Fin du tableau
    html += `
              </tbody>
            </table>
          </div>
        `;

    // On affiche
    $(".etablissement-content").html(html);
    $(".etablissement-content").show();
    $(".carte").hide();
  });
  request.fail(function (request) {
    casErreur(request);
  });
  request.always(function () {});
}


// Fonction qui permet d'ajouter un etablissement à la base de données
function ajoutEtablissement() {
  if (champsVerifFail()) {
    alerteErreur();
  } else {
    let request = $.ajax({
      type: "POST",
      url: "http://localhost/api/v1/etablissement",
      dataType: "json",
      data: {
        nom: $("#nom").val(),
        type_etablissement: $("#type_etablissement_form").val(),
        mail: $("#mail").val(),
        tel: $("#tel").val(),
        adresse: $("#adresse").val(),
        horaire: $("#horaire").val(),
        description: $("#description").val()
      },
      beforeSend: function () {},
    });
    request.done(function (reponse) {
      $(".sucessMsg").text("Ajout d'un établissement avec succès");
      $(".sucessMsg").show()
      $(".sucessMsg").removeClass("alert-danger")
      $(".sucessMsg").addClass("alert-success")
      resetForm();
      setTimeout(() => {
          $(".sucessMsg").text("");
          $(".sucessMsg").hide();
        }, 3000);
    });
    request.fail(function (request) {
      alert("Adresse saisie invalide");
      //casErreur(request); Temporaire
    });
    request.always(function () {});
  }
}


// Fonction qui recupere les informations sur l'etablissement que l'on veut modifier
function modifEtablissement(id) {
  let request = $.ajax({
    type: "GET",
    url: `http://localhost/api/v1/etablissement?id=${id}`,
    beforeSend: function () {
    },
  });
  request.done(function (rep) {
    response = rep[0]
    $("#old_id").val(response.id);
    $("#nom").val(response.nom);
    $("#type_etablissement").val(response.type);
    $("#mail").val(response.mail);
    $("#tel").val(response.tel);
    $("#adresse").val(response.adresse);
    $("#horaire").val(response.horaire);
    $("#description").val(response.description);

    $(".form-titre").text("Modification de l'établissement");
    $(".ajout-form").show();
    $(".maj-btn").show();
    $(".ajout-btn").hide();
    $(".sucessMsg").hide();
    $(".etablissement-content").hide();
  });
  request.fail(function (request) {
    alert("Adresse saisie invalide");
    // casErreur(request); Temporaire
  });
  request.always(function () {});
}


// Fonction qui permet de mettre à jour les informations sur un etablissement
function majEtablissement() {
  if (champsVerifFail()) {
    alerteErreur();
  } else {
    let request = $.ajax({
      type: "PUT",
      url: `http://localhost/api/v1/etablissement`,
      dataType: "json",
      data: {
        id: $("#old_id").val(),
        nom: $("#nom").val(),
        type_etablissement: $("#type_etablissement_form").val(),
        mail: $("#mail").val(),
        tel: $("#tel").val(),
        adresse: $("#adresse").val(),
        horaire: $("#horaire").val(),
        description: $("#description").val()
      },
      beforeSend: function () {},
    });
    request.done(function (response) {
      etablissement();
    });
    request.fail(function (request) {
      casErreur(request);
    });
    request.always(function () {});
  }
}


// Fonction qui permet de supprimer un etablissement
function suppEtablissement(id){
  let request = $.ajax({
      type: "DELETE",
      url: `http://localhost/api/v1/etablissement?id=${id}`,
      beforeSend: function () {},
    });
    request.done(function (response) {
      console.log("well done")
      etablissement()
    });
    request.fail(function (request) {
      // casErreur(request);
    });
    request.always(function () {
      console.log("cheating");
      etablissement()
    });
}


// Fonction qui permet d'afficher les détails d'un etablissement
function afficheEtablissement(id) {
  let request = $.ajax({
    type: "GET",
    url: `http://localhost/api/v1/etablissement?id=${id}`,
    beforeSend: function () {
    },
  });
  request.done(function (res) {
    // On recoit une liste de 1 élément et on veut le premier élément
    response = res[0]
    html = ` <div class="p-4 col-md-10 offset-md-1">
              <h3 class="p-4 text-center">${response.nom}</h3>
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <th scope="row">Id</th>
                    <td>${response.id}</td>
                  </tr>
                  <tr>
                    <th scope="row">Nom</th>
                    <td>${response.nom}</td>
                  </tr>
                  <tr>
                    <th scope="row">Type d'établissement</th>
                    <td>${response.type}</td>
                  </tr>
                  <tr>
                    <th scope="row">Mail</th>
                    <td>${response.mail}</td>
                  </tr>
                  <tr>
                    <th scope="row">Tel</th>
                    <td>${response.tel}</td>
                  </tr>
                  <tr>
                    <th scope="row">Adresse</th>
                    <td>${response.adresse}</td>
                  </tr>
                  <tr>
                    <th scope="row">GPS</th>
                    <td>Latitude: ${response.geolat},  Longitude: ${response.geolng}</td>
                  </tr>
                  <tr>
                    <th scope="row">Description</th>
                    <td>${response.horaire}</td>
                  </tr>
                  <tr>
                    <th scope="row">Description</th>
                    <td>${response.description}</td>
                  </tr>
                </tbody>
              </table>
              <button type="button" class="btn btn-primary px-4" onclick="cacheEtablissement()">  Cacher Etablissement  </button>
            </div>
          `;
          test = ".numProfil"+id
          $(".affichageProfil").hide();
          $(test).html(html);
          $(test).show();
  });
  request.fail(function (request) {
    casErreur(request);
  });
  request.always(function () {});
}


// Fonction qui permet de cacher les dtails d'un etablissement
function cacheEtablissement(){
  $(".affichageProfil").hide();
}


// ------------------------------------
//
//  Formulaire
//
// ------------------------------------


function afficheFormulaire() {
  $(".etablissement-content").hide();
  $(".ajout-form").show();
  $(".sucessMsg").hide();
  $(".accueil").hide();
  $(".carte").hide();
  $(".form-titre").text("Ajout d'un établissement");
  $(".ajout-btn").show();
  $(".maj-btn").hide();
  resetForm();
}


function resetForm() {
  $("#old_id").val("");
  $("#nom").val("");
  $("#mail").val("");
  $("#tel").val("");
  $("#adresse").val("");
  $("#horaire").val("");
  $("#description").val("");
}


// ------------------------------------
//
//  Factorisation du code
//
// ------------------------------------


function showOnlyContent(response) {
  $(".etablissement-content").html(response);
  $(".etablissement-content").show();
  $(".accueil").hide();
  $(".carte").hide();
  $(".ajout-form").hide()
}


function casErreur(request) {
  alert("Erreur : " + request.status + " - " + request.statusText);
}


function champsVerifFail() {
  const mailformat = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
  const telformat = new RegExp(/^((\+)33|0)[1-9](\d{2}){4}$/);
  let formatNonRespecte = ( !($("#mail").val().match(mailformat)) || !($("#tel").val().match(telformat)));
  let champsNonRemplis = ($("#nom").val() == ""  || $("#mail").val() == "" || $("#tel") == "" || $("#adresse").val() == "" || $("#horaire").val() == "" || $("#description").val() == "");

  let msg = champsNonRemplis ? "Veuillez remplir tous les champs" : formatNonRespecte ? "Veuillez respecter le format des champs" : "";

  return msg;
}


function alerteErreur() {
  $(".sucessMsg").text(champsVerifFail());
  $(".sucessMsg").show()
  $(".sucessMsg").removeClass("alert-success")
  $(".sucessMsg").addClass("alert-danger")

  setTimeout(() => {
    $(".sucessMsg").text("");
    $(".sucessMsg").hide();
  }, 3000);
}

