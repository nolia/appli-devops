import os

print("remove app container")
os.system('docker stop app')
os.system('docker rm app')

print("remove sql container")
os.system('docker stop sql')
os.system('docker rm sql')

os.system('docker image prune -a -f')
os.system('docker volume prune')

os.system('docker-compose up -d')
